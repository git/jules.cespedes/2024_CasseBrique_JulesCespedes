class Anime extends Sprite {

  public xmin_ : number;
  public xmax_ : number;
  public ymin_ : number;
  public ymax_ : number;

  public constructor(element : HTMLElement) {
    super(element);

    this.xmin_ = 0;
    this.xmax_ = 0;
    this.ymin_ = 0;
    this.ymax_ = 0;
  }

  public setLimites(zone : Sprite) {

    this.xmin_ = zone.getX();
    this.xmax_ = zone.getX() + zone.getWidth() - this.getWidth();
    this.ymin_ = zone.getY();
    this.ymax_ = zone.getY() + zone.getHeight() - this.getHeight();
  }

  public animer() {


  }

  public figer() {

  }
}