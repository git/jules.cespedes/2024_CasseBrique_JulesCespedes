class Palet extends Anime {

  private ecouteurSuivre : any;

  public constructor(element : HTMLElement) {
    super(element);
    this.setImage("palet.png",700,200);

    this.ecouteurSuivre = (evt : MouseEvent) => {this.suivre(evt)};
  }

  public override animer() {
    window.addEventListener("mousemove", this.ecouteurSuivre);
  }

  public override figer() {
    window.removeEventListener("mousemove", this.ecouteurSuivre);
  }

  private suivre(evt : MouseEvent) {
    let nx : number = evt.clientX - this.getParent().getX() - this.getWidth()/2;

    if (nx <= this.xmin_) nx = this.xmin_;
    if (nx >= this.xmax_) nx = this.xmax_;

    this.setX(nx);
  }

}