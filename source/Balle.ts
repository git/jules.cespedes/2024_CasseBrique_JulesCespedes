class Balle extends Anime {
  //public xmin_ : number;
  //public xmax_ : number;
  //public ymin_ : number;
  //public ymax_ : number;

  public vx : number;
  public vy : number;
  public vitesseinitiale : number;
  public multiplicateur : number;
  public vitesseMax : number;
  private coller : boolean;
  public ecouteurSouris : any;

  public timerAnimation : number;
  private scene_ : Jeu;




  public constructor(element : HTMLElement, scene : Jeu) {
    super(element);
    this.setImage("balle.png",50,50);

    //this.xmin_ = 0;
    //this.xmax_ = 0;
    //this.ymin_ = 0;
    //this.ymax_ = 0;
    //this.timerAnimation = 0;

    this.vitesseinitiale= 5;

    this.initVitesse(this.vitesseinitiale);

    this.scene_ = scene;
    this.coller = true;

    this.ecouteurSouris = (evt : MouseEvent) => (this.coller = false);


  }

  public override animer() {
    this.timerAnimation = setInterval( () => {this.bouger()},1000/60);
    window.addEventListener("mousedown", this.ecouteurSouris)
  }

  public override figer() {
    clearInterval(this.timerAnimation);
    window.removeEventListener("mousedown", this.ecouteurSouris)
  }

  private nx_ : number;
  private ny_ : number;

  public bouger() {

    if (this.coller) {
      this.nx_= this.scene_.palet_.getX() + this.scene_.palet_.getWidth()/2 - this.getWidth()/2;
      this.ny_= this.scene_.palet_.getY() - this.getHeight();

      this.setXY(this.nx_,this.ny_)
    }
    else if (this.scene_.compteurScore_ >= this.scene_.brique_.length) {
      this.figer();
      this.scene_.palet_.figer();
      this.scene_.partieGagnee();
    }

    else {

    this.nx_ = this.getX() + this.vx;
    this.ny_ = this.getY() + this.vy;

    this.rebondirBordure();
    this.rebondirPalet();
    this.rebondirBriques();

    this.setXY(this.nx_, this.ny_);
    }


}


  private vitesse() {
    this.vitesseMax = 8;
    this.multiplicateur = 1.05;
    this.vx *= this.multiplicateur;
    this.vy*= this.multiplicateur;
    if (this.getVitesse() >= this.vitesseMax) {
      this.vx = this.vitesseMax;
      this.vy = this.vitesseMax;
    }
  }

  private rebondirBordure() {
        if (this.nx_ >= this.xmax_) {
          this.nx_ = this.xmax_;
          this.vx *= -1;
      }

      else if (this.nx_ <= this.xmin_) {
          this.nx_ = this.xmin_
          this.vx *= -1 ;
      }

      if (this.ny_ >= this.ymax_) {
        this.figer();
        this.scene_.palet_.figer();
        this.scene_.partiePerdue();
      }

      else if (this.ny_ <= this.ymin_) {
        this.ny_ = this.ymin_;
        this.vy *=-1 ;
      }

  }

  private rebondirPalet() {

      if (Balle.collision(this.getCircle(),this.scene_.palet_.getRectangle())) {
        this.ny_ = this.scene_.palet_.getY() - this.getHeight();
        this.vy *= -1;

        this.vitesse();
      };

  }

  private rebondirBriques() {

    for (let i : number = 0; i<this.scene_.brique_.length ; i++) {
      if (Balle.collision(this.getCircle(),this.scene_.brique_[i].getRectangle())) {
        this.scene_.augmenterScore();

        //this.ny_ = this.scene_.brique_[i].getY() - this.getHeight();
        this.scene_.brique_[i].setXY(-1000000,-1000000);
        this.scene_.removeChild(this.scene_.brique_[i]);
        this.vy *= -1;
        //this.scene_.brique_[i] = null;
      }
    }

  }

  public initVitesse(v: number) {
    this.vx = 4*Math.random() -2;
    this.vy = -2;
    let vr : number = Math.sqrt(this.vx*this.vx + this.vy*this.vy);

    this.vx = this.vx/vr * v;
    this.vy = this.vy/vr * v;

  }

  public getVitesse() {
    return Math.sqrt(this.vx*this.vx + this.vy * this.vy);
  }
}