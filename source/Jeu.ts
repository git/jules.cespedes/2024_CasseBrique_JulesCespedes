//==================================================================================================
// ANIMATION AVEC TYPESCRIPT                                                                 Jeu.ts
//==================================================================================================

// Classe  J e u //---------------------------------------------------------------------------------
class Jeu extends Scene {
 //----------------------------------------------------------------------------------------Attributs
 /* Declarer ici les attributs de la scene. */
public balle_ : Balle;
public palet_ : Palet;
public brique_ : Array<Sprite>;

public score_ : HTMLElement;
public scoreFin_ : HTMLElement;
public compteurScore_ : number;

public defaite_ : HTMLElement;
public victoire_ : HTMLElement;

public recommencer_ : HTMLElement;
public pause_ : HTMLElement;
public textePause_ : HTMLElement;
public unpause_ : HTMLElement;

public zone_ : Sprite;

 //-------------------------------------------------------------------------------------Constructeur
 public constructor(element : HTMLElement) {
  super(element,false);
  /* Ecrire ici le code qui initialise la scene. */

        //Création de la zone de jeu
        this.zone_= new Sprite (document.getElementById("jeu"));
        this.zone_.setXY(10,10);
        this.zone_.setWidth(this.getWidth()-20);
        this.zone_.setHeight(this.getHeight()-20);

        //Gestion des stats
        this.compteurScore_ = 0;
        this.score_ = document.getElementById('score');
        this.score_.textContent = "Score : " + this.compteurScore_;

        this.scoreFin_ = document.getElementById('scoreI');

        //Gestion de la partie
        this.defaite_ = document.getElementById('defaite');
        this.defaite_.style.display = 'none';

        this.victoire_ = document.getElementById('victoire');
        this.victoire_.style.display = 'none';

        this.recommencer_ = document.getElementById('recommencer');
        this.pause_ = document.getElementById('pause');
        this.textePause_ = document.getElementById('textepause');
        this.unpause_ = document.getElementById('unpause');

        this.clique();



 }

 //--------------------------------------------------------------------------------------------start
 public override start() {




      //Création de la balle
  this.balle_ = new Balle (document.createElement("img"), this);
  this.balle_.setImage("balle.png",25,25);
  this.balle_.setXY(this.getWidth() /2 - this.balle_.getWidth() /2, this.getHeight() /2 - this.balle_.getHeight() /2);
  this.appendChild(this.balle_);

      //Paramètre de la balle
          //let balle2 : Balle = new Balle(document.createElement("img"));
          //this.appendChild(balle2);
          this.balle_.setLimites(this.zone_);
          this.balle_.setXY(this.balle_.xmax_, this.balle_.ymax_);

          //setTimeout( () => {this.balle_.figer()},1000);

      //Création du palet
  this.palet_ = new Palet (document.createElement("img"));
  this.palet_.setImage("palet.png",100,15);
  this.palet_.setLimites(this.zone_);
  this.palet_.setXY(this.getWidth() /2 - this.palet_.getWidth() /2, this.getHeight() - 75);
  this.appendChild(this.palet_);



  this.brique_ = [];

  let nbColonnes : number = 5;
  let nbLignes : number = 4;
  let sx : number = this.zone_.getWidth()/ (nbColonnes+1);
  let sy : number = this.zone_.getHeight()*0.4 / (nbLignes+1);

  for (let i : number = 0; i < nbLignes; i++) {

    for (let j : number = 0; j < nbColonnes; j++) {
      let brique = new Sprite(document.createElement("div"));
      brique.setDimension(40,24);
      brique.getElement().className = "brique";

      brique.setImage("brique.png",40,24);
      brique.setX((j+1) * sx + this.zone_.getX() - brique.getWidth()/2);
      brique.setY((i+1) * sy + this.zone_.getY() - brique.getHeight()/2);

      this.appendChild(brique);
      this.brique_.push(brique);
  }
}



  this.palet_.animer();
  this.balle_.animer();





 }

 public augmenterScore() {
  this.compteurScore_ += 1;
  this.score_.textContent = "Score : " + this.compteurScore_;
 }

 public partieGagnee() {
  this.scoreFin_.textContent = "Votre score : " + this.compteurScore_ + "/" + this.brique_.length;
  this.victoire_.style.display = "block";
 }

 public partiePerdue() {
  this.scoreFin_.textContent = "Votre score : " + this.compteurScore_ + "/" + this.brique_.length;
  this.defaite_.style.display = "block";
 }

 public clique() {
  this.recommencer_.addEventListener('click',this.recommencer.bind(this));
  this.pause_.addEventListener('click',this.pause.bind(this));
  this.unpause_.addEventListener('click',this.unpause.bind(this));
 }

 public recommencer() {
  this.clean();
  setTimeout(() => {
    this.start();
  }, 1500);

 }

 //--------------------------------------------------------------------------------------------pause
 public override pause() {
  /* Ecrire ici le code qui met la scene en pause. */
    this.textePause_.style.display = "block";
    this.unpause_.style.display = "block";
    this.pause_.style.display = "none";
    this.balle_.figer();
    this.palet_.figer();
 }

 //------------------------------------------------------------------------------------------unpause
 public override unpause() {
  /* Ecrire ici le code qui sort la scene de la pause. */
  this.textePause_.style.display = "none";
  this.unpause_.style.display = "none";
  this.pause_.style.display = "block";

  this.balle_.animer();
  this.palet_.animer();
 }

 //--------------------------------------------------------------------------------------------clean
 public override clean() {
  /* Ecrire ici le code qui nettoie la scene en vue d'un redemarrage. */
  this.defaite_.style.display = "none";
  this.victoire_.style.display = "none";

  this.compteurScore_ = 0;
  this.score_.textContent = "Score : " + this.compteurScore_;

  this.removeChild(this.balle_);
  this.removeChild(this.palet_);

  for (let i : number = 0; i < this.brique_.length; i++) {
    this.brique_[i].setXY(-1000000,-1000000);
  }
 }
}

// Fin //-------------------------------------------------------------------------------------------
